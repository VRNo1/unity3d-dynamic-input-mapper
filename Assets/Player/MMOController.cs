using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(CharacterMotor))]

public class MMOController : MonoBehaviour
{
    public string axisX = "None";
    public string axisY = "None";
    public AnimationClip walkAnimations;
    public AnimationClip runAnimations;
    public AnimationClip jumpAnimations;
    public AnimationClip doubleJumpAnimations;
    public AnimationClip sitAnimations;
    public AnimationClip standAnimations;
    public AnimationClip swimAnimations;
    public AnimationClip idleSwimAnimations;

    private InputManager inputManager;
    private GameObject character;
    private CharacterMotor motor;
    private MMOCameraController mmocamera;

    private float currentMoveSpeedMultiplier;
    private bool guibuttonpressed = false;

    private bool isflying;
    private bool isswimming;
    //private GUIManager guiManager;
    public float runspeedmultiplier = 3;
    public float walkspeedmultiplier = 1;

    public void Awake()
    {
        currentMoveSpeedMultiplier = runspeedmultiplier;
        motor = (CharacterMotor)this.GetComponent(typeof(CharacterMotor));
        inputManager = new InputManager();
        axisX = inputManager.axisX;
        axisY = inputManager.axisY;
        //guiManager = GameObject.Find("Main Camera").GetComponent<GUIManager>();
        mmocamera = GameObject.Find("Main Camera").GetComponent<MMOCameraController>();
        character = this.gameObject;
        //foreach (InputHandler i in inputManager.InputKeys)
        //{
        //    Debug.Log(i.Command.ToString());
        //}
    }

    void OnGUI()
    {
        //Event e = Event.current;
        //if (e.isKey)
        //{
        //    Debug.Log(e.keyCode);
        //}
    }

    public void Update()
    {
        inputManager.CheckControleKyes();
        Vector3 dir = new Vector3();
        //if (guiManager)
        //{
        //    if ((guiManager.showCharacterMenu || guiManager.showInventory || guiManager.showMainMenu || guiManager.showQuestlog || guiManager.showSocialMenu || guiManager.showSpellBook || guiManager.showTalents || guiManager.showWorldMap) || guibuttonpressed)
        //    {
        //        mmocamera.guiMode = true;
        //        guiManager.showGUI = true;
        //    }
        //    else if ((!guiManager.showCharacterMenu || !guiManager.showInventory || !guiManager.showMainMenu || !guiManager.showQuestlog || !guiManager.showSocialMenu || !guiManager.showSpellBook || !guiManager.showTalents || !guiManager.showWorldMap) && guiManager.showGUI)
        //    {
        //        guiManager.showGUI = false;
        //        mmocamera.guiMode = false;
        //    }
        //    else
        //    {
        //        guiManager.showGUI = false;
        //        mmocamera.guiMode = false;
        //    }
        //    if (guiManager.showOptionsMenu || guiManager.showKeyBindingsMenu || guiManager.showLogOutMenu || guiManager.showQuitMenu)
        //    {
        //        if (Input.GetKeyDown(KeyCode.Escape))
        //        {
        //            guiManager.showKeyBindingsMenu = false;
        //            guiManager.showOptionsMenu = false;
        //        }
        //        motor.inputMoveDirection = Vector3.zero;
        //        return;
        //    }

        //    if (guiManager.showMainMenu)
        //    {
        //        if (Input.GetKeyDown(KeyCode.Escape))
        //        {
        //            guiManager.showMainMenu = !guiManager.showMainMenu;
        //        }
        //        motor.inputMoveDirection = Vector3.zero;
        //        return;
        //    }
        //}
        ////Menu statements
        //if (Input.GetKeyDown(KeyCode.Escape))
        //{
        //    guiManager.showMainMenu = !guiManager.showMainMenu;
        //}
        //if (inputManager.CheckInput("Toggle Gui Mode", KeyStages.Down))
        //{
        //    if (guiManager.showCharacterMenu || guiManager.showInventory || guiManager.showMainMenu || guiManager.showQuestlog || guiManager.showSocialMenu || guiManager.showSpellBook || guiManager.showTalents || guiManager.showWorldMap)
        //    {
        //        guibuttonpressed = false;
        //        mmocamera.guiMode = false;
        //        guiManager.showCharacterMenu = false;
        //        guiManager.showGUI = false;
        //        guiManager.showInventory = false;
        //        guiManager.showMainMenu = false;
        //        guiManager.showQuestlog = false;
        //        guiManager.showSocialMenu = false;
        //        guiManager.showSpellBook = false;
        //        guiManager.showTalents = false;
        //        guiManager.showWorldMap = false;
        //    }
        //    else
        //    {
        //        guibuttonpressed = !guibuttonpressed;
        //        mmocamera.guiMode = !mmocamera.guiMode;
        //        guiManager.showGUI = !guiManager.showGUI;
        //    }
        //}
        //if (inputManager.CheckInput("Actionbar 1", KeyStages.Down))
        //{

        //}
        //if (inputManager.CheckInput("Inventory", KeyStages.Down))
        //{
        //    guiManager.showInventory = !guiManager.showInventory;
        //}
        //if (inputManager.CheckInput("Quest", KeyStages.Down))
        //{
        //    guiManager.showQuestlog = !guiManager.showQuestlog;
        //}
        //if (inputManager.CheckInput("Character", KeyStages.Down))
        //{
        //    guiManager.showCharacterMenu = !guiManager.showCharacterMenu;
        //}
        //if (inputManager.CheckInput("SpellBook", KeyStages.Down))
        //{
        //    guiManager.showSpellBook = !guiManager.showSpellBook;
        //}
        //if (inputManager.CheckInput("WorldMap", KeyStages.Down))
        //{
        //    guiManager.showWorldMap = !guiManager.showWorldMap;
        //}
        //if (inputManager.CheckInput("Social", KeyStages.Down))
        //{
        //    guiManager.showSocialMenu = !guiManager.showSocialMenu;
        //}
        //if (inputManager.CheckInput("Talents", KeyStages.Down))
        //{
        //    guiManager.showTalents = !guiManager.showTalents;
        //}

        //Movement statements
        if (inputManager.CheckInput("Move Forward", KeyStages.Press) || Input.GetAxis(axisY) < -0.04f)
        {
            dir.z += 1;
        }
        if (inputManager.CheckInput("Move Backward", KeyStages.Press) || Input.GetAxis(axisY) > 0.04f)
        {
            dir.z -= 1;
        }
        if (inputManager.CheckInput("Strafe Left", KeyStages.Press) || Input.GetAxis(axisX) < -0.04f)
        {
            dir.x -= 1;
        }
        if (inputManager.CheckInput("Strafe Right", KeyStages.Press) || Input.GetAxis(axisX) > 0.04f)
        {
            dir.x += 1;
        }
        
        if (inputManager.CheckInput("Toggle Run/Walk", KeyStages.Down))
        {
            if (currentMoveSpeedMultiplier == runspeedmultiplier)
            {
                currentMoveSpeedMultiplier = walkspeedmultiplier;
            }
            else
            {
                currentMoveSpeedMultiplier = runspeedmultiplier;
            }
        }

        if (inputManager.CheckInput("Interact", KeyStages.Down))
        {
            Ray mouseRay = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            RaycastHit hit;

            if (Physics.Raycast(mouseRay, out hit, 10))
            {
                if (hit.transform.gameObject.GetComponent(typeof(IInteract)) != null)
                {
                    (hit.transform.gameObject.GetComponent(typeof(IInteract)) as IInteract).Interact(hit.transform.gameObject);
                }
            }
        }

        motor.inputJump = inputManager.CheckInput("Jump", KeyStages.Down);
        //if (motor.swimming.isSwimming)
        //{
        //    dir.y = Mathf.Sin(mmocamera.transform.rotation.ToEulerAngles().x) * -1;
        //    Debug.Log(dir.y);
        //}
        motor.inputMoveDirection = this.transform.TransformDirection(dir * currentMoveSpeedMultiplier);
    }

    void OnApplicationQuit()
    {
        PlayerPrefs.DeleteAll();
    }
}