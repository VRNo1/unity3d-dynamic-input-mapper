using UnityEngine;
using System.Collections;

public class MMOCameraController : MonoBehaviour
{
    public Transform target;

    public float targetHeight;
    public float distance = 5.0f;
    public float offsetFromWall = 0.1f;

    public float maxDistance = 20;
    public float minDistance = 0f;

    public float xSpeed = 200.0f;
    public float ySpeed = 200.0f;

    public int yMinLimit = -90;
    public int yMaxLimit = 90;

    public int zoomRate = 40;

    public float rotationDampening = 5.0f;
    public float zoomDampening = 5.0f;

    public LayerMask collisionLayers = -1;

    public string joystickAxisX = "NONE";
    public string joystickAxisY = "NONE";

    public int invertX;
    public int invertY;

    [HideInInspector]
    public bool guiMode = false;

    private float xDeg = 0.0f;
    private float yDeg = 0.0f;
    private float currentDistance;
    private float desiredDistance;
    private float correctedDistance;

    void Start()
    {
        Vector3 angles = transform.eulerAngles;
        xDeg = angles.x;
        yDeg = angles.y;

        currentDistance = distance;
        desiredDistance = distance;
        correctedDistance = distance;

        // Make the rigid body not change rotation 
        if (rigidbody)
            rigidbody.freezeRotation = true;
        
        
        
    }

    void Update()
    {
        //Physics.Raycast(
        Quaternion rotation = Quaternion.Euler(yDeg, xDeg, 0);
        Vector3 vTargetOffset;
        vTargetOffset = new Vector3(0, -targetHeight, 0);
        RaycastHit collisionHit;
        Vector3 position = target.position - (rotation * Vector3.forward * desiredDistance + vTargetOffset);
        Vector3 trueTargetPosition = new Vector3(target.position.x, target.position.y + targetHeight, target.position.z);

        // if there was a collision, correct the camera position and calculate the corrected distance 
        bool isCorrected = false;
        if (Physics.Linecast(trueTargetPosition, position, out collisionHit, collisionLayers))
        {
            // calculate the distance from the original estimated position to the collision location,
            // subtracting out a safety "offset" distance from the object we hit.  The offset will help
            // keep the camera from being right on top of the surface we hit, which usually shows up as
            // the surface geometry getting partially clipped by the camera's front clipping plane.
            correctedDistance = Vector3.Distance(trueTargetPosition, collisionHit.point) - offsetFromWall;
            isCorrected = true;
        }

        // For smoothing, lerp distance only if either distance wasn't corrected, or correctedDistance is more than currentDistance 
        currentDistance = !isCorrected || correctedDistance > currentDistance ? Mathf.Lerp(currentDistance, correctedDistance, Time.deltaTime * zoomDampening) : correctedDistance;

        // keep within legal limits
        currentDistance = Mathf.Clamp(currentDistance, minDistance, maxDistance);
    }

    /** 
     * Camera logic on LateUpdate to only update after all character movement logic has been handled. 
     */
    void LateUpdate()
    {
        // check for collision using the true target's desired registration point as set by user using height 
        Vector3 vTargetOffset;
        
        // Don't do anything if target is not defined 
        if (!target)
            return;

        // If either mouse buttons are down, let the mouse govern camera position 
        //if (Input.GetMouseButton(0) ^ Input.GetMouseButton(1))
        //{
        if (!guiMode)
        {
            xDeg += Input.GetAxis("Mouse X") * xSpeed * 0.1f * invertX;
            yDeg -= Input.GetAxis("Mouse Y") * ySpeed * 0.1f * invertY;
            xDeg += Input.GetAxis(joystickAxisX) * xSpeed * invertX;
            yDeg -= Input.GetAxis(joystickAxisY) * ySpeed * invertY;
			//Debug.Log(xDeg + yDeg);
        }
        //}
        // otherwise, ease behind the target if any of the directional keys are pressed 
        //else if (Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") != 0)
        //{
            float targetRotationAngle = target.eulerAngles.y;
            float currentRotationAngle = transform.eulerAngles.y;
            //xDeg = Mathf.LerpAngle(currentRotationAngle, targetRotationAngle, rotationDampening * Time.deltaTime);
        //}

        yDeg = ClampAngle(yDeg, yMinLimit, yMaxLimit);

        // set camera rotation 
        Quaternion rotation = Quaternion.Euler(yDeg, xDeg, 0);
        
        // calculate the desired distance 
        if (!guiMode)
        {
            desiredDistance -= Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime * zoomRate * 10;
        }
        desiredDistance = Mathf.Clamp(desiredDistance, minDistance, maxDistance);
        correctedDistance = desiredDistance;

        // calculate desired camera position
        vTargetOffset = new Vector3(0, -targetHeight, 0);
        

        // recalculate position based on the new currentDistance 
        Vector3 position = target.position - (rotation * Vector3.forward * currentDistance + vTargetOffset);

        transform.rotation = rotation;
        //if (Input.GetMouseButton(1))
        //{
            target.rotation = new Quaternion(0, rotation.y, 0, rotation.w);
        //}
            position.y += 1;
        transform.position = position;
    }

    private static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360)
            angle += 360;
        if (angle > 360)
            angle -= 360;
        return Mathf.Clamp(angle, min, max);
    }
}