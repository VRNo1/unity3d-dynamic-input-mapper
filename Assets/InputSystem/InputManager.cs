using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

public class InputManager
{
	public string axisX = "NONE";
	public string axisY = "NONE";
	public List<InputHandler> InputKeys;
	private SortedDictionary<KeyCode, bool> Controlekeys = new SortedDictionary<KeyCode, bool> { { KeyCode.LeftControl, false }, { KeyCode.RightControl, false }, { KeyCode.LeftAlt, false }, { KeyCode.RightAlt, false }, { KeyCode.AltGr, false }, { KeyCode.LeftShift, false }, { KeyCode.RightShift, false } };
	private List<string> Commands = new List<string>() { "Move Forward", "Move Backward", "Strafe Left", "Strafe Right", 
		"Jump", "Toggle Run/Walk", "Toggle Gui Mode", "QuickActionbar 1", "QuickActionbar 2", "QuickActionbar 3", "Actionbar 1", "Actionbar 2", 
		"Actionbar 3", "Actionbar 4", "Actionbar 5", "Actionbar 6", "Actionbar 7", "Actionbar 8", "Actionbar 9", "Actionbar 10", 
		"Actionbar 11", "Actionbar 12", "Interact", "Inventory", "Quest", "Character", "SpellBook", "WorldMap", "Social", "Talents" };

	public InputManager()
	{
		InputKeys = new List<InputHandler>();
		foreach (string s in Commands)
		{
			if (!PlayerPrefs.HasKey(s))
			{
                CreateConfig();
			}
		}
		//if (!Directory.Exists(Application.dataPath + "\\Config\\"))
		//{
		//    Directory.CreateDirectory(Application.dataPath + "\\Config\\");
		//}
		//if (!File.Exists(Application.dataPath + "\\Config\\InputConfig.ini"))
		//{
		//    CreateConfig();
		//}
		LoadConfig();
	}

	void CreateConfig()
	{
		PlayerPrefs.DeleteAll();
		//StreamWriter file = new StreamWriter(Application.dataPath + "\\Config\\InputConfig.ini", false);
		// Command = Key1; Key2; Joystick Button/Key

		PlayerPrefs.SetString("Move Forward", "W; UpArrow; NONE");
		PlayerPrefs.SetString("Move Backward", "S; DownArrow; NONE");
		PlayerPrefs.SetString("Strafe Left", "A; LeftArrow; NONE");
		PlayerPrefs.SetString("Strafe Right", "D; RightArrow; NONE");
		PlayerPrefs.SetString("Jump", "Space; NONE; NONE");

		PlayerPrefs.SetString("Toggle Run/Walk", "Numlock; Slash; NONE");
		PlayerPrefs.SetString("Toggle Gui Mode", "G; NONE; NONE");


		PlayerPrefs.SetString("QuickActionbar 1", "Mouse0; NONE; NONE");
		PlayerPrefs.SetString("QuickActionbar 2", "Mouse1; NONE; NONE");
		PlayerPrefs.SetString("QuickActionbar 3", "Mouse2; NONE; NONE");

		PlayerPrefs.SetString("Actionbar 1", "Alpha1; NONE; NONE");
		PlayerPrefs.SetString("Actionbar 2", "Alpha2; NONE; NONE");
		PlayerPrefs.SetString("Actionbar 3", "Alpha3; NONE; NONE");
		PlayerPrefs.SetString("Actionbar 4", "Alpha4; NONE; NONE");
		PlayerPrefs.SetString("Actionbar 5", "Alpha5; NONE; NONE");
		PlayerPrefs.SetString("Actionbar 6", "Alpha6; NONE; NONE");
		PlayerPrefs.SetString("Actionbar 7", "Alpha7; NONE; NONE");
		PlayerPrefs.SetString("Actionbar 8", "Alpha8; NONE; NONE");
		PlayerPrefs.SetString("Actionbar 9", "Alpha9; NONE; NONE");
		PlayerPrefs.SetString("Actionbar 10", "Alpha0; NONE; NONE");
		PlayerPrefs.SetString("Actionbar 11", "Minus; NONE; NONE");
		PlayerPrefs.SetString("Actionbar 12", "Equals; NONE; NONE");

		PlayerPrefs.SetString("Interact", "F; NONE; NONE");
		PlayerPrefs.SetString("Inventory", "B; NONE; NONE");
		PlayerPrefs.SetString("Quest", "L; NONE; NONE");
		PlayerPrefs.SetString("Character", "C; NONE; NONE");
		PlayerPrefs.SetString("SpellBook", "P; NONE; NONE");
		PlayerPrefs.SetString("WorldMap", "M; NONE; NONE");
		PlayerPrefs.SetString("Social", "O; NONE; NONE");
		PlayerPrefs.SetString("Talents", "N; NONE; NONE");

		PlayerPrefs.Save();



		//file.WriteLine("Move Forward = W; UpArrow; NONE");
		//file.WriteLine("Move Backward = S; DownArrow; NONE");
		//file.WriteLine("Strafe Left = A; LeftArrow; NONE");
		//file.WriteLine("Strafe Right = D; RightArrow; NONE");
		//file.WriteLine("Jump = Space; NONE; NONE");

		//file.WriteLine("Toggle Run/Walk = Numlock; Slash; NONE");
		//file.WriteLine("Toggle Gui Mode = G; NONE; NONE");
		

		//file.WriteLine("QuickActionbar 1 = Mouse0; NONE; NONE");
		//file.WriteLine("QuickActionbar 2 = Mouse1; NONE; NONE");
		//file.WriteLine("QuickActionbar 3 = Mouse2; NONE; NONE");

		//file.WriteLine("Actionbar 1 = Alpha1; NONE; NONE");
		//file.WriteLine("Actionbar 2 = Alpha2; NONE; NONE");
		//file.WriteLine("Actionbar 3 = Alpha3; NONE; NONE");
		//file.WriteLine("Actionbar 4 = Alpha4; NONE; NONE");
		//file.WriteLine("Actionbar 5 = Alpha5; NONE; NONE");
		//file.WriteLine("Actionbar 6 = Alpha6; NONE; NONE");
		//file.WriteLine("Actionbar 7 = Alpha7; NONE; NONE");
		//file.WriteLine("Actionbar 8 = Alpha8; NONE; NONE");
		//file.WriteLine("Actionbar 9 = Alpha9; NONE; NONE");
		//file.WriteLine("Actionbar 10 = Alpha0; NONE; NONE");
		//file.WriteLine("Actionbar 11 = Minus; NONE; NONE");
		//file.WriteLine("Actionbar 12 = Equals; NONE; NONE");

		//file.WriteLine("Interact = F; NONE; NONE");
		//file.WriteLine("Inventory = B; NONE; NONE");
		//file.WriteLine("Quest = L; NONE; NONE");
		//file.WriteLine("Character = C; NONE; NONE");
		//file.WriteLine("SpellBook = P; NONE; NONE");
		//file.WriteLine("WorldMap = M; NONE; NONE");
		//file.WriteLine("Social = O; NONE; NONE");
		//file.WriteLine("Talents = N; NONE; NONE");

		//file.Flush();
		//file.Close();
	}

	void LoadConfig()
	{
		//StreamReader file = new StreamReader(Application.dataPath + "\\Config\\InputConfig.ini");
		InputKeys = new List<InputHandler>();
		try
		{
			foreach (string command in Commands)
			{
				if (!PlayerPrefs.HasKey(command))
				{
					CreateConfig();
				}
				else
				{
					string[] inputs = PlayerPrefs.GetString(command).Trim().Split(';');
					List<string> keys1 = new List<string>();
					List<string> keys2 = new List<string>();
					List<string> keys3 = new List<string>();
					keys1.AddRange(inputs[0].Split('+'));
					keys2.AddRange(inputs[1].Split('+'));
					keys3.AddRange(inputs[2].Split('+'));
					if (keys3[0].Contains("JoyStickAxis") && (command == "MoveForward" || command == "MoveBackward"))
					{
						axisY = keys3[0];
						keys3 = new List<string>() { "NONE" };
					}
					if (keys3[0].Contains("JoyStickAxis") && (command == "StrafeLeft" || command == "StrafeRight"))
					{
						axisX = keys3[0];
						keys3 = new List<string>() { "NONE" };
					}
					InputKeys.Add(new InputHandler(command, keys1.ToArray(), keys2.ToArray(), keys3.ToArray()));
				}
			}
		}
		catch (Exception ex)
		{
			Debug.Log("Error in InputConfig.ini, Generating a new one..." + ex.Message);
		}
		//try
		//{
		//    //TODO: add handler to check if all commands is added.
		//    int i = 0;
		//    while (!file.EndOfStream)
		//    {
		//        string read = file.ReadLine();
		//        string trim = "";
		//        foreach (string s in read.Split(' '))
		//        {
		//            trim += s;
		//        }

		//        string[] commands = trim.Split('=');
		//        string command = commands[0];
		//        string[] inputs = commands[1].Split(';');
		//        Debug.Log(command);
		//        if (inputs.Length == 3 && command.ToLower() == Commands[i].ToLower())
		//        {
		//            List<string> keys1 = new List<string>();
		//            List<string> keys2 = new List<string>();
		//            List<string> keys3 = new List<string>();
		//            keys1.AddRange(inputs[0].Split('+'));
		//            keys2.AddRange(inputs[1].Split('+'));
		//            keys3.AddRange(inputs[2].Split('+'));
		//            if (keys3[0].Contains("JoyStickAxis") && (command == "MoveForward" || command == "MoveBackward"))
		//            {
		//                axisY = keys3[0];
		//                keys3 = new List<string>() { "NONE" };
		//            }
		//            if (keys3[0].Contains("JoyStickAxis") && (command == "StrafeLeft" || command == "StrafeRight"))
		//            {
		//                axisX = keys3[0];
		//                keys3 = new List<string>() { "NONE" };
		//            }
		//            InputKeys.Add(new InputHandler(command, keys1.ToArray(), keys2.ToArray() , keys3.ToArray()));
		//        }
		//        else
		//        {
		//            throw new Exception("Error in InputConfig.ini, Generating a new one...");
		//        }
		//        i++;
		//    }
		//    file.Close();
		//}
		//catch (Exception ex)
		//{
		//    file.Close();
		//    Debug.Log("Error in InputConfig.ini, Generating a new one..." + ex.Message);
		//    File.Delete(Application.dataPath + "\\Config\\InputConfig.ini");
		//    CreateConfig();
		//    LoadConfig();
		//}
	}

	public void SaveConfig(string asixX, string asixY)
	{
        //StreamWriter file = new StreamWriter(Application.dataPath + "\\Config\\InputConfig.ini", false);
        //foreach (InputHandler h in InputKeys)
        //{
        //    if (asixX.Contains("JoyStickAxis") && asixY.Contains("JoyStickAxis") && (h.Command == "MoveForward" || h.Command == "MoveBackward" || h.Command == "StrafeLeft" || h.Command == "StrafeRight"))
        //    {
        //        if (h.Command == "MoveForward" || h.Command == "MoveBackward")
        //        {
        //            file.WriteLine(h.JoyStickAxis(asixY));
        //        }
        //        if (h.Command == "StrafeLeft" || h.Command == "StrafeRight")
        //        {
        //            file.WriteLine(h.JoyStickAxis(asixX));
        //        }
        //    }
        //    else
        //    {
        //        file.WriteLine(h.ToString());
        //    }
        //}
        //file.Flush();
        //file.Close();
	}

	public void ChangeKeys(string _command, KeyCode kc, KeyStoreType kst)
	{
		InputHandler ih = (InputHandler)InputKeys.Select(i => i.Command == _command);
		if (kst == KeyStoreType.Key1)
		{
			ih.Key1.RemoveRange(0, ih.Key1.Count - 1);
			ih.Key1.Add(kc);
			for (int i = 0; i < Controlekeys.Count; i++)
			{
				if (Controlekeys.Values.ElementAt(i) == true)
				{
					ih.Key1.Add(Controlekeys.Keys.ElementAt(i));
				}
			}
		}
		else if (kst == KeyStoreType.Key2)
		{
			ih.Key2.RemoveRange(0, ih.Key1.Count - 1);
			ih.Key2.Add(kc);
			for(int i = 0; i < Controlekeys.Count; i++)
			{
				if (Controlekeys.Values.ElementAt(i) == true)
				{
					ih.Key2.Add(Controlekeys.Keys.ElementAt(i));
				}
			}
		}
		else if (kst == KeyStoreType.Joystick)
		{
			ih.Joystick.RemoveRange(0, ih.Key1.Count - 1);
			ih.Joystick.Add(kc);
		}
	}
	
	public void CheckControleKyes()
	{
		int j = Controlekeys.Count;
		for (int i = 0; i < j; i++)
		{
			if (Input.GetKeyDown(Controlekeys.Keys.ElementAt(i)))
			{
				Controlekeys[Controlekeys.Keys.ElementAt(i)] = true;
			}
			if (Input.GetKeyUp(Controlekeys.Keys.ElementAt(i)))
			{
				Controlekeys[Controlekeys.Keys.ElementAt(i)] = false;
			}
		}
	}

	public bool CheckInput(string command, KeyStages stage)
	{
        InputHandler inputChecker = InputKeys.Find(f => f.Command == command);
        if (stage == KeyStages.Press)
        {
            if (ListCheckerPress(inputChecker.Key1))
            {
                return true;
            }
            else if (ListCheckerPress(inputChecker.Key2))
            {
                return true;
            }
            else if (ListCheckerPress(inputChecker.Joystick))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else if (stage == KeyStages.Down)
        {
            if (ListCheckerDown(inputChecker.Key1))
            {
                return true;
            }
            else if (ListCheckerDown(inputChecker.Key2))
            {
                return true;
            }
            else if (ListCheckerDown(inputChecker.Joystick))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else if (stage == KeyStages.Up)
        {
            if (ListCheckerUp(inputChecker.Key1))
            {
                return true;
            }
            else if (ListCheckerUp(inputChecker.Key2))
            {
                return true;
            }
            else if (ListCheckerUp(inputChecker.Joystick))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        return false;
	}

	private bool ListCheckerPress(List<KeyCode> list)
	{
		bool checkval = false;
		foreach (KeyCode k in list)
		{
			if (Controlekeys.ContainsKey(k))
			{
				if (Controlekeys[k])
				{
					checkval = true;
					continue;
				}
			}
			if (Input.GetKey(k))
			{
				checkval = true;
			}
			else
			{
				return false;
			}
		}
		return checkval;
	}

	private bool ListCheckerUp(List<KeyCode> list)
	{
		bool checkval = false;
		foreach (KeyCode k in list)
		{
			if (Controlekeys.ContainsKey(k))
			{
				if (Controlekeys[k])
				{
					checkval = true;
					continue;
				}
			}
			if (Input.GetKeyUp(k))
			{
				checkval = true;
			}
			else
			{
				return false;
			}
		}
		return checkval;
	}

	private bool ListCheckerDown(List<KeyCode> list)
	{
		bool checkval = false;
		foreach (KeyCode k in list)
		{
			if (Controlekeys.ContainsKey(k))
			{
				if (Controlekeys[k])
				{
					checkval = true;
					continue;
				}
			}
			if (Input.GetKeyDown(k))
			{
				checkval = true;
			}
			else
			{
				return false;
			}
		}
		return checkval;
	}
}
